# Archive and compress

Tutorial to archive and compress files and directories on the console

Between `[` `]` is optional

Install

```
# apt install y zip unzip tar gzip bzip2 xz-utils
```

## Zip

Zip is a archive and compress system

Compress

```
$ zip archive.zip directory
```

Uncompress

```
$ unzip [-d directory] archive.zip
```

## Tar

Tar is a archive system, not compress

Some flags

```
c = create
x = extract
r = add file
t = list
v = verbose
f = file or dir
C = output
j = bzip2
J = xz
z = Gzip
```

Create

```
$ tar -cvf archive.tar directory
```

List content

```
$ tar -tvf archive.tar
```

Extract all

```
$ tar [-C directory] -xvf archive.tar
```

Extract specific file or directory

```
$ tar -xvf archive.tar file.txt
```

Add file or directory

```
$ tar -rvf archive.tar file.txt
```

Delete file or directory

```
$ tar --delete file.txt -vf archive.tar
```

## Tar + Gzip

Archive and compress with Tar and Gzip (.tar.gz or .tgz)

Compress

```
$ tar -zcvf archive.tar.gz directory
```

Unpack

```
$ tar [-C directory] -zxvf archive.tar.gz
```

## Tar + Bzip2

Archive and compress with Tar and Bzip2 (.tar.bz2 or .tbz)

Compress

```
$ tar -jcvf archive.tar.bz2 directory
```

Unpack

```
$ tar [-C directory] -jxvf archive.tar.bz2
```

## Tar + Xz

Archive and compress with Tar and Xz (.tar.xz)

Compress

```
$ tar -Jcvf archive.tar.xz directory
```

Unpack

```
$ tar [-C directory] -Jxvf archive.tar.xz
```

## Gzip

Compress with Gzip (.gz)

Compress

```
$ gzip file.txt
// file.tar to file.txt.gz
```

Uncompress

```
$ gunzip file.txt.gz
// file.txt.gz to file.txt
```

## Bzip2

Compress with (.bz2)

Compress

```
$ bzip2 file.txt
// file.txt to file.txt.bz2
```

Uncompress

```
$ bunzip2 file.txt.bz2
// file.txt.bz2 to file.txt
```

## Xz

Compress

```
$ xz file.txt
// file.txt to file.txt.xz
```

Uncompress

```
$ unxz file.txt.xz
// file.txt.xz to file.txt
```
